<link rel="stylesheet" type="text/css" href="../css/menu.css">
<nav id="separateNav">
  <ul class="navLinks alignNav">
    <li><a alt="Accueil" href="index.php#">Accueil</a></li>
    <li><a alt="Programmation" href="programmation.php#">Programmation</a></li>
    <li><a alt="Réseau" href="reseau.php">Réseau</a></li>
    <li><a alt="Matériel" href="materiel.php">Matériel</a></li>
    <li><a alt="Mathématiques" href="mathematiques.php">Mathématiques</a></li>
    <li><a alt="Electricité" href="electricite.php">Electricité</a></li>
    <li><a alt="Conseils" href="conseils.php">Conseils</a><li>
  </ul>
</nav>
