<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>FNI</title>
        <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\InfoFNP\Formation\css\index.css">
    </head>
    <body>
    <div id="corps" class="margins">
        <h1>Informatique, guerre de la formation</h1>
        <div id="presentation" class="txts">
            <h2>Présentation</h2>
            <p id="p1">
               Aide-Mémoire pour les cours d'informatiques ou liés à l'informatique.
                <br />
                <br />
                <h3>Site web</h3> 
            </p>
        </div>    
        <div id="codeSource" class="txts spacingB">
            <h2>Code source</h2>
            <a href="https://gitlab.com/info-suisse-libre/apprentissage-informatique">
                <img src="../img/GitLab_Logo.svg">
            </a>
        </div>
    </div>
    <?php include("menu.php"); include("pied.php"); ?>
    </body>
</html>