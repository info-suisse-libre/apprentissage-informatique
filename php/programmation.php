<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Programmation</title>
        <link rel="stylesheet" type="text/css" href="../css/programmation.css">
    </head>
    <body>
    <div id="corps" class="margins">
        <h1 id="programmation">Programmation</h1>
        <div class="txts">
            <ul class="ulNum">
                <li><a href="#Languages"><span class="liNum">1</span><span class="linksToTitles">Languages</span></a>
                <ul class="ulNum"><li><a href="#Pseudo_Code"><span class="liNum">2</span><span class="linksToTitles">Pseudo Code</span></a></li> 
                <li><a href="#Language_C"><span class="liNum">3</span><span class="linksToTitles">Language C</span></a></li>    
                <li><a href="#Language_Java"><span class="liNum">4</span><span class="linksToTitles">Language Java</span></a></li>
                <li><a href="#Language_Python"><span class="liNum">5</span><span class="linksToTitles">Language Python</span></a></li>
                </ul>    
            </li>
            </ul>
        </div>
    </div>
    <?php include("menu.php"); include("pied.php");?>
    </body>
</html>
